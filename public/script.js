window.addEventListener("DOMContentLoaded", function () {
    const PRICE_1 = 350; 
    const PRICE_2 = 950;
    const PRICE_3 = 400;
	const PRICE_4 = 300;
    const EXTRA_PRICE = 0;
    const RADIO_1 = 0; 
    const RADIO_2 = 4515;
    const RADIO_3 = 9880;
    const CHECKBOX = 280;
		
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    let calc = document.getElementById("calc"); 
    let myNum = document.getElementById("number"); 
    let resultSpan = document.getElementById("result");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
	
    select.addEventListener("change", function (event) {
        let option = event.target;
        if (option.value === "1") {
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_1;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = RADIO_1;
            price = PRICE_2;
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_3;
            checkbox.checked = false;
        }
    });
    radios.forEach(function (currentRadio) {//
        currentRadio.addEventListener("change", function (event) {//
            let radio = event.target;//
            if (radio.value === "r1") {//
                extraPrice = RADIO_1;//
            }
            if (radio.value === "r2") {//
                extraPrice = RADIO_2;//
            }
            if (radio.value === "r3") {//
                extraPrice = RADIO_3;//
            }
        });
    });
    checkbox.addEventListener("change", function () {//
        if (checkbox.checked) {
            extraPrice = CHECKBOX;//
        }		
        else {
            extraPrice = EXTRA_PRICE;//
        }
    });
	
    calc.addEventListener("change", function () {//
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        result = (price + extraPrice) * myNum.value;//
        resultSpan.innerHTML = result;//
    });
});




